<?php
class LinkMetaController extends DrupalDefaultEntityController{
    public function load($ids = array(), $conditions = array()) {
        $entities = array();
        
        $passed_ids = !empty($ids) ? array_flip($ids) : FALSE;
        $entities += $this->cacheGet($ids, $conditions);
        if ($passed_ids) {
            $ids = array_keys(array_diff_key($passed_ids, $entities));
        }
        
        
        if ($ids === FALSE || $ids || ($conditions && !$passed_ids)) {
            $query = $this->buildQuery($ids, $conditions);
            $queried_entities = $query
                ->execute()
                ->fetchAllAssoc($this->idKey, 'LinkMeta');
        }
        
        if (!empty($queried_entities)) {
            $this->attachLoad($queried_entities);
            $entities += $queried_entities;
        }
        
        if (!empty($queried_entities)) {
            $this->cacheSet($queried_entities);
        }
        
        if ($passed_ids) {
            $passed_ids = array_intersect_key($passed_ids, $entities);
            foreach ($entities as $entity) {
                $passed_ids[$entity->{$this->idKey}] = $entity;
            }
            $entities = $passed_ids;
        }
        
        return $entities;
    }
}