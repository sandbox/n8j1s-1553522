<?php
class LinkMeta {
    public $lid = 0;
    public $scheme = '';
    public $host = '';
    public $port = NULL;
    public $path = '';
    public $query = '';
    public $fragment = '';
    public $username = '';
    public $password = '';
    public $uid = NULL;
    public $created = 0;
    public $changed = 0;
    public $type = '';
    
    public static function parseURL($url){
        $comps = parse_url($url);
        $ret = new LinkMeta();
        foreach($comps as $key=>$value){
            $ret->{$key} = $value;
        }
        return $ret;
    }
    
    public function getComponents(){
        return array(
            'scheme' => $this->scheme,
            'host' => $this->host,
            'port' => $this->port,
            'path' => $this->path,
            'query' => $this->query,
            'fragment' => $this->fragment,
            'username' => $this->username,
            'password' => $this->password,
        );
    }
}